package com.nimbusds.openid.connect.provider.spi.grants.client.handler;


import com.thetransactioncompany.util.PropertyFilter;
import junit.framework.TestCase;
import net.minidev.json.JSONObject;
import org.junit.After;

import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.SoftwareID;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.grants.GrantAuthorization;


public class SimpleClientCredentialsGrantHandlerTest extends TestCase {
	
	
	@After
	@Override
	public void tearDown() {
		
		var filtered = PropertyFilter.filterWithPrefix(Configuration.PREFIX, System.getProperties());
		filtered.stringPropertyNames().forEach(System::clearProperty);
	}


	public void testRequestWithScope()
		throws Exception {

		var scope = Scope.parse("read write admin");

		var clientID = new ClientID("123");
		var clientMetadata = new ClientMetadata();
		clientMetadata.setScope(new Scope("read", "write"));

		InitContext initCtx = new MockInitContext();

		var handler = new SimpleClientCredentialsGrantHandler();
		handler.init(initCtx);
		assertEquals(GrantType.CLIENT_CREDENTIALS, handler.getGrantType());

		GrantAuthorization authzOut = handler.processGrant(scope, clientID, clientMetadata);
		assertEquals(Scope.parse("read write"), authzOut.getScope());
		assertEquals("http://s1.example.com", authzOut.getAudience().get(0).getValue());
		assertEquals(3600L, authzOut.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertFalse(authzOut.getAccessTokenSpec().encrypt());
		assertNull(authzOut.getData());
	}


	public void testRequestWithInvalidScope()
		throws Exception {

		var scope = Scope.parse("admin browse");

		var clientID = new ClientID("123");
		var clientMetadata = new ClientMetadata();
		clientMetadata.setScope(new Scope("read", "write"));

		InitContext initCtx = new MockInitContext();

		var handler = new SimpleClientCredentialsGrantHandler();
		handler.init(initCtx);
		assertEquals(GrantType.CLIENT_CREDENTIALS, handler.getGrantType());

		try {
			handler.processGrant(scope, clientID, clientMetadata);
			fail();
		} catch (GeneralException e) {

			assertEquals(OAuth2Error.INVALID_SCOPE, e.getErrorObject());
		}
	}


	public void testRequestWithNoScope()
		throws Exception {

		var clientID = new ClientID("123");
		var clientMetadata = new ClientMetadata();
		clientMetadata.setScope(new Scope("read", "write"));

		InitContext initCtx = new MockInitContext();

		var handler = new SimpleClientCredentialsGrantHandler();
		handler.init(initCtx);
		assertEquals(GrantType.CLIENT_CREDENTIALS, handler.getGrantType());

		var authzOut = handler.processGrant(null, clientID, clientMetadata);
		assertEquals(Scope.parse("read write"), authzOut.getScope());
		assertEquals("http://s1.example.com", authzOut.getAudience().get(0).getValue());
		assertEquals(3600L, authzOut.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertFalse(authzOut.getAccessTokenSpec().encrypt());
	}


	public void testRequestWithEmptyScope()
		throws Exception {

		var clientID = new ClientID("123");
		var clientMetadata = new ClientMetadata();
		clientMetadata.setScope(new Scope("read", "write"));

		InitContext initCtx = new MockInitContext();

		var handler = new SimpleClientCredentialsGrantHandler();
		handler.init(initCtx);
		assertEquals(GrantType.CLIENT_CREDENTIALS, handler.getGrantType());

		var authzOut = handler.processGrant(new Scope(), clientID, clientMetadata);
		assertEquals(Scope.parse("read write"), authzOut.getScope());
		assertEquals("http://s1.example.com", authzOut.getAudience().get(0).getValue());
		assertEquals(3600L, authzOut.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertFalse(authzOut.getAccessTokenSpec().encrypt());
	}


	public void testOverrideEnable()
		throws Exception {

		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.enable", "false");

		InitContext initCtx = new MockInitContext();

		var handler = new SimpleClientCredentialsGrantHandler();
		handler.init(initCtx);

		assertFalse(handler.isEnabled());
	}


	public void testOverrideTokenLifetime()
		throws Exception {

		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime", "100");

		InitContext initCtx = new MockInitContext();

		var handler = new SimpleClientCredentialsGrantHandler();
		handler.init(initCtx);

		assertEquals(100L, handler.getConfiguration().accessToken.lifetime);
	}
	
	
	public void testIncludeClientMetadataFields()
		throws Exception {
		
		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.includeClientMetadataFields", "software_id data.org_id");
		
		var scope = Scope.parse("read write admin");
		
		var clientID = new ClientID("123");
		var clientMetadata = new ClientMetadata();
		clientMetadata.setScope(new Scope("read", "write"));
		clientMetadata.setSoftwareID(new SoftwareID("9ac9159d-30fc-482f-bd89-4bf9f32ddf78"));
		var data = new JSONObject();
		data.put("org_id", "123");
		clientMetadata.setCustomField("data", data);
		
		InitContext initCtx = new MockInitContext();
		
		var handler = new SimpleClientCredentialsGrantHandler();
		handler.init(initCtx);
		assertEquals(GrantType.CLIENT_CREDENTIALS, handler.getGrantType());
		
		var authzOut = handler.processGrant(scope, clientID, clientMetadata);
		assertEquals(Scope.parse("read write"), authzOut.getScope());
		assertEquals("http://s1.example.com", authzOut.getAudience().get(0).getValue());
		assertEquals(3600L, authzOut.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertFalse(authzOut.getAccessTokenSpec().encrypt());
		
		var expectedData = new JSONObject();
		expectedData.put("software_id", clientMetadata.getSoftwareID().getValue());
		expectedData.put("org_id", ((JSONObject)clientMetadata.getCustomField("data")).get("org_id"));
		assertEquals(expectedData, authzOut.getData());
	}
}
