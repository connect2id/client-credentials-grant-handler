package com.nimbusds.openid.connect.provider.spi.grants.client.handler;


import java.util.List;
import java.util.Properties;
import java.util.Set;

import com.thetransactioncompany.util.PropertyFilter;
import junit.framework.TestCase;

import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;


public class ConfigurationTest extends TestCase {
	
	
	@Override
	public void tearDown()
		throws Exception {
		
		super.tearDown();
		
		var propsToClear = PropertyFilter.filterWithPrefix(Configuration.PREFIX, System.getProperties());
		
		for (var propName: propsToClear.stringPropertyNames()) {
			System.clearProperty(propName);
		}
	}


	public void testConstructFromProperties() {

		var props = new Properties();
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.enable", "true");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime", "3600");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding", "SELF_CONTAINED");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encrypt", "false");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.audienceList", "");

		var config = new Configuration(props);

		config.log();

		assertTrue(config.enable);
		assertEquals(3600L, config.accessToken.lifetime);
		assertEquals(TokenEncoding.SELF_CONTAINED, config.accessToken.encoding);
		assertFalse(config.accessToken.encrypt);
		assertNull(config.accessToken.audienceList);
		assertTrue(config.accessToken.includeClientMetadataFields.isEmpty());
	}


	public void testConstructFromSystemProperties() {

		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.enable", "true");
		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime", "3600");
		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding", "SELF_CONTAINED");
		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encrypt", "false");
		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.audienceList", "");

		var config = new Configuration(new Properties());

		config.log();

		assertTrue(config.enable);
		assertEquals(3600L, config.accessToken.lifetime);
		assertEquals(TokenEncoding.SELF_CONTAINED, config.accessToken.encoding);
		assertFalse(config.accessToken.encrypt);
		assertNull(config.accessToken.audienceList);
		assertTrue(config.accessToken.includeClientMetadataFields.isEmpty());
	}


	public void testConstructFromProperties_includeClientMetadata() {

		var props = new Properties();
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.enable", "true");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime", "3600");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding", "SELF_CONTAINED");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encrypt", "false");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.audienceList", "");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.includeClientMetadataFields", "software_id data.org_id");

		var config = new Configuration(props);

		config.log();

		assertTrue(config.enable);
		assertEquals(3600L, config.accessToken.lifetime);
		assertEquals(TokenEncoding.SELF_CONTAINED, config.accessToken.encoding);
		assertFalse(config.accessToken.encrypt);
		assertNull(config.accessToken.audienceList);
		assertEquals(Set.of("software_id", "data.org_id"), config.accessToken.includeClientMetadataFields);
	}


	public void testConstructFromSystemProperties_includeClientMetadata() {

		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.enable", "true");
		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime", "3600");
		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding", "SELF_CONTAINED");
		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encrypt", "false");
		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.audienceList", "");
		System.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.includeClientMetadataFields", "software_id data.org_id");

		var config = new Configuration(new Properties());

		config.log();

		assertTrue(config.enable);
		assertEquals(3600L, config.accessToken.lifetime);
		assertEquals(TokenEncoding.SELF_CONTAINED, config.accessToken.encoding);
		assertFalse(config.accessToken.encrypt);
		assertNull(config.accessToken.audienceList);
		assertEquals(Set.of("software_id", "data.org_id"), config.accessToken.includeClientMetadataFields);
	}


	public void testConstructFromPropertiesWithAudienceList() {

		var props = new Properties();
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.enable", "true");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime", "3600");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding", "SELF_CONTAINED");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encrypt", "true");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.audienceList", "http://rs1.example.com http://rs2.example.com");

		var config = new Configuration(props);

		config.log();

		assertTrue(config.enable);
		assertEquals(3600L, config.accessToken.lifetime);
		assertEquals(TokenEncoding.SELF_CONTAINED, config.accessToken.encoding);
		assertTrue(config.accessToken.encrypt);
		assertEquals("http://rs1.example.com", config.accessToken.audienceList.get(0).getValue());
		assertEquals("http://rs2.example.com", config.accessToken.audienceList.get(1).getValue());
		assertEquals(2, config.accessToken.audienceList.size());
		assertTrue(config.accessToken.includeClientMetadataFields.isEmpty());
	}


	public void testConstructFromPropertiesWithIdentifierBasedAccessToken() {

		var props = new Properties();
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.enable", "true");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime", "3600");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding", "IDENTIFIER");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encrypt", "true");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.audienceList", "http://rs1.example.com http://rs2.example.com");

		var config = new Configuration(props);

		config.log();

		assertTrue(config.enable);
		assertEquals(3600L, config.accessToken.lifetime);
		assertEquals(TokenEncoding.IDENTIFIER, config.accessToken.encoding);
		assertFalse(config.accessToken.encrypt);
		assertEquals(List.of(new Audience("http://rs1.example.com"), new Audience("http://rs2.example.com")), config.accessToken.audienceList);
		assertTrue(config.accessToken.includeClientMetadataFields.isEmpty());
	}


	public void testDisabledByDefault() {

		var config = new Configuration(new Properties());
		
		assertFalse(config.enable);
		
		assertNull(config.accessToken);
		
		config.log();
	}
}