package com.nimbusds.openid.connect.provider.spi.grants.client.handler;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Properties;
import javax.servlet.ServletContext;

import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.ServiceContext;
import org.infinispan.manager.EmbeddedCacheManager;


/**
 * Mock initialisation context.
 */
class MockInitContext implements InitContext {


	@Override
	public InputStream getResourceAsStream(final String path) {

		Properties props = new Properties();
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.enable", "true");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime", "3600");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding", "SELF_CONTAINED");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.encrypt", "false");
		props.setProperty("op.grantHandler.clientCredentials.simpleHandler.accessToken.audienceList", "http://s1.example.com");

		ByteArrayOutputStream os = new ByteArrayOutputStream();

		try {
			props.store(os, null);

		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}

		return new ByteArrayInputStream(os.toByteArray());
	}
	
	
	@Override
	public ServletContext getServletContext() {
		return null;
	}
	
	
	@Override
	public EmbeddedCacheManager getInfinispanCacheManager() {
		return null;
	}
	
	
	@Override
	public Issuer getIssuer() {
		return null;
	}
	
	
	@Override
	public Issuer getOPIssuer() {
		return null;
	}
	
	
	@Override
	public URI getTokenEndpointURI() {
		return null;
	}
	
	
	@Override
	public ServiceContext getServiceContext() {
		return null;
	}
}
