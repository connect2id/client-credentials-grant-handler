package com.nimbusds.openid.connect.provider.spi.grants.client.handler;


import java.util.Optional;
import java.util.Properties;

import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.grants.AccessTokenSpec;
import com.nimbusds.openid.connect.provider.spi.grants.ClientCredentialsGrantHandler;
import com.nimbusds.openid.connect.provider.spi.grants.GrantAuthorization;


/**
 * Simple client credentials grant handler. Determines the authorised scope by
 * looking up the registered scope values in the OAuth 2.0 client metadata.
 */
public class SimpleClientCredentialsGrantHandler implements ClientCredentialsGrantHandler {


	/**
	 * The configuration file path.
	 */
	public static final String CONFIG_FILE_PATH = "/WEB-INF/clientGrantHandler.properties";


	/**
	 * The configuration.
	 */
	private Configuration config;
	
	
	/**
	 * The client metadata filter.
	 */
	private ClientMetadataFilter clientMetadataFilter;


	/**
	 * Loads the configuration.
	 *
	 * @param initContext The initialisation context. Must not be
	 *                    {@code null}.
	 *
	 * @return The configuration.
	 *
	 * @throws Exception If loading failed.
	 */
	private static Configuration loadConfiguration(final InitContext initContext)
		throws Exception {
		
		var props = new Properties();
		
		var inputStream = initContext.getResourceAsStream(CONFIG_FILE_PATH);
		
		if (inputStream != null) {
			props.load(inputStream);
		}

		return new Configuration(props);
	}


	@Override
	public void init(final InitContext initContext)
		throws Exception {

		Loggers.MAIN.info("[CGH0000] Initializing client credentials grant handler...");
		config = loadConfiguration(initContext);
		config.log();
		
		if (config.enable) {
			clientMetadataFilter = new ClientMetadataFilter(config.accessToken.includeClientMetadataFields);
		}
	}


	/**
	 * Returns the configuration.
	 *
	 * @return The configuration.
	 */
	public Configuration getConfiguration() {

		return config;
	}


	@Override
	public GrantType getGrantType() {

		return GrantType.CLIENT_CREDENTIALS;
	}


	@Override
	public boolean isEnabled() {

		return config.enable;
	}


	@Override
	public GrantAuthorization processGrant(final Scope scope,
					       final ClientID clientID,
					       final ClientMetadata clientMetadata)
		throws GeneralException {

		if (! config.enable) {
			throw new GeneralException("Grant handler disabled", OAuth2Error.UNSUPPORTED_GRANT_TYPE);
		}

		Loggers.TOKEN_ENDPOINT.debug("[CGH0002] Client credentials grant handler: Received request with scope={}", scope);

		var registeredScopeValues = clientMetadata.getScope();

		if (CollectionUtils.isEmpty(registeredScopeValues)) {
			throw new GeneralException("Access denied", OAuth2Error.INVALID_SCOPE);
		}


		Scope authorizedScope;

		if (CollectionUtils.isEmpty(scope)) {
			// Default to registered scope values
			authorizedScope = registeredScopeValues;
		} else {
			// Discard non-registered scope values
			authorizedScope = scope;
			authorizedScope.retainAll(registeredScopeValues);

			if (authorizedScope.isEmpty()) {
				// No scope values match
				throw new GeneralException("Access denied", OAuth2Error.INVALID_SCOPE);
			}
		}

		return new GrantAuthorization(authorizedScope,
			new AccessTokenSpec(
				config.accessToken.lifetime,
				config.accessToken.audienceList,
				config.accessToken.encoding,
				Optional.of(config.accessToken.encrypt)),
			clientMetadataFilter.filter(clientMetadata));
	}


	@Override
	public void shutdown() {

		// Nothing to do

		Loggers.MAIN.info("[CGH0003] Shutting down client credentials grant handler...");
	}
}
