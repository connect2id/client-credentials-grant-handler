package com.nimbusds.openid.connect.provider.spi.grants.client.handler;


import java.util.*;

import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;

import com.nimbusds.common.config.ConfigurationException;
import com.nimbusds.common.config.LoggableConfiguration;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;


/**
 * Client credentials grant handler configuration. It is typically derived from
 * a Java key / value properties file. The configuration is stored as public
 * fields which become immutable (final) after their initialisation.
 *
 * <p>Example configuration properties:
 *
 * <pre>
 * op.grantHandler.clientCredentials.simpleHandler.enable=true
 * op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime=3600
 * op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding=SELF_CONTAINED
 * op.grantHandler.clientCredentials.simpleHandler.accessToken.encrypt=false
 * op.grantHandler.clientCredentials.simpleHandler.accessToken.audienceList=http://rs1.example.com http://rs2.example.com
 * op.grantHandler.clientCredentials.simpleHandler.accessToken.includeClientMetadataFields=software_id data.org_id
 * </pre>
 */
public final class Configuration implements LoggableConfiguration {


	/**
	 * The properties prefix.
	 */
	public static final String PREFIX = "op.grantHandler.clientCredentials.simpleHandler.";


	/**
	 * Enables / disables the client credentials grant handler.
	 *
	 * <p>Property key: [prefix]enable
	 */
	public final boolean enable;


	/**
	 * Access token settings for successfully authorised client credentials
	 * grants.
	 */
	public static class AccessToken implements LoggableConfiguration {


		/**
		 * The access token lifetime in seconds.
		 *
		 * <p>Property key: [prefix]lifetime
		 */
		public final long lifetime;


		/**
		 * The access token encoding.
		 *
		 * <p>Property key: [prefix]encoding
		 */
		public final TokenEncoding encoding;


		/**
		 * Enables / disables encryption of self-contained (JWT-
		 * encoded) access tokens.
		 *
		 * <p>Property key: [prefix]encrypt
		 */
		public final boolean encrypt;


		/**
		 * The audience for the access tokens, {@code null} if not
		 * specified.
		 *
		 * <p>Property key: [prefix]audienceList
		 */
		public final List<Audience> audienceList;
		
		
		/**
		 * Names of client metadata fields to include in the optional
		 * access token "data" field, empty set if none. To specify a
		 * member within a field that is a JSON object member use dot
		 * (.) notation.
		 *
		 * <p>Property key: [prefix]includeClientMetadataFields
		 */
		public final Set<String> includeClientMetadataFields;


		/**
		 * Creates a new access token configuration from the specified
		 * properties.
		 *
		 * @param prefix The properties prefix. Must not be
		 *               {@code null}.
		 * @param props  The properties. Must not be {@code null}.
		 *
		 * @throws PropertyParseException On a missing or invalid
		 *                                property.
		 */
		public AccessToken(final String prefix, final Properties props)
			throws PropertyParseException {

			var pr = new PropertyRetriever(props, true);

			lifetime = pr.getLong(prefix + "lifetime");

			encoding = pr.getEnum(prefix + "encoding", TokenEncoding.class);

			if (encoding.equals(TokenEncoding.SELF_CONTAINED)) {
				encrypt = pr.getBoolean(prefix + "encrypt");
			} else {
				encrypt = false;
			}
			
			audienceList = Audience.create(pr.getOptStringList(prefix + "audienceList", null));
			
			includeClientMetadataFields = new HashSet<>(pr.getOptStringList(prefix + "includeClientMetadataFields", Collections.emptyList()));
		}


		@Override
		public void log() {
			
			Loggers.MAIN.info("[CGH0101] Client credentials grant handler access token lifetime: {} s", lifetime);
			Loggers.MAIN.info("[CGH0102] Client credentials grant handler access token encoding: {}", encoding);

			if (encoding.equals(TokenEncoding.SELF_CONTAINED)) {
				Loggers.MAIN.info("[CGH0103] Client credentials grant handler access token encrypt: {}", encrypt);
			}

			if (audienceList != null) {
				Loggers.MAIN.info("[CGH0104] Client credentials grant handler access token audience: {}", audienceList);
			}
			
			if (CollectionUtils.isNotEmpty(includeClientMetadataFields)) {
				Loggers.MAIN.info("[CGH0105] Client credentials grant handler client metadata fields to include in access tokens: {}", includeClientMetadataFields);
			}
		}
	}


	/**
	 * The access token settings.
	 */
	public final AccessToken accessToken;


	/**
	 * Creates a new client credentials grant handler configuration from
	 * the specified properties.
	 *
	 * @param props The properties. Must not be {@code null}.
	 *
	 * @throws ConfigurationException On a missing or invalid property.
	 */
	public Configuration(final Properties props)
		throws ConfigurationException {

		var pr = new PropertyRetriever(props, true);

		try {
			enable = pr.getOptBoolean(PREFIX + "enable", false);
			
			if (enable) {
				accessToken = new AccessToken(PREFIX + "accessToken.", props);
			} else {
				accessToken = null;
			}

		} catch (PropertyParseException e) {

			throw new ConfigurationException(e.getMessage() + ": Property: " + e.getPropertyKey());
		}
	}


	/**
	 * Logs the configuration details at INFO level. Properties that may
	 * adversely affect security are logged at WARN level.
	 */
	@Override
	public void log() {

		Loggers.MAIN.info("[CGH0100] Client credentials grant handler enabled: {}", enable);
		
		if (enable) {
			accessToken.log();
		}
	}
}
