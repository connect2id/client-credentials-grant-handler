/**
 * Simple handler of client credential grants.
 */
package com.nimbusds.openid.connect.provider.spi.grants.client.handler;