# Client credentials grant handler for the Connect2id server

Copyright (c) Connect2id Ltd., 2014 - 2022

Simple Connect2id server plugin for [handling client credentials 
grants](https://connect2id.com/products/server/docs/integration/client-credentials-grant-handler)
(also see RFC 6749, section 4.4).

* Requires [Connect2id server](https://connect2id.com/products/server) v7.11+.

* The scope of the access token scope is bound by the registered scope values 
  for the requesting client (the `scope` client metadata field).
  
* Allows setting of the optional token `data` from selected client metadata 
  fields.


## Configuration

The handler is configured from Java system properties or from properties in the 
optional `/WEB-INF/clientGrantHandler.properties` file. The Java system 
properties have precedence. If no configuration properties are found the 
handler is disabled.

* `op.grantHandler.clientCredentials.simpleHandler.enable` -- If `true` enables 
  the client credentials grant handler. Disabled (`false`) by default.

* `op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime` -- The 
  access token lifetime, in seconds.

* `op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding` -- The 
  access token encoding:
    * `IDENTIFIER` -- The access token is a secure identifier. The associated
      authorisation is looked up by a call to the Connect2id server token
      introspection endpoint.
    * `SELF_CONTAINED` -- Self-contained access token. The associated
      authorisation is encoded in the access token itself, as a signed and 
      optionally encrypted JSON Web Token (JWT). Can also be looked up by a 
      call to the Connect2id server token introspection endpoint.

* `op.grantHandler.clientCredentials.simpleHandler.accessToken.encrypt` -- If
  `true` enables additional encryption of self-contained (JWT-encoded) access 
  tokens.

* `op.grantHandler.clientCredentials.simpleHandler.accessToken.audienceList` --
  Optional audience for the access tokens, as comma and / or space separated 
  list of values.

* `op.grantHandler.clientCredentials.simpleHandler.accessToken.includeClientMetadataFields` --
  Names of client metadata fields to include in the optional access token 
  `data` field, empty set if none. To specify a member within a field that is a 
  JSON object member use dot (`.`) notation.


Example minimal configuration for issuing self-contained (JWT) access tokens 
with a lifetime of 10 minutes (600 seconds):

```ini
op.grantHandler.clientCredentials.simpleHandler.enable=true
op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime=600
op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding=SELF_CONTAINED
```

Example configuration for issuing identifier-based access tokens which are
checked at the Connect2id server [token introspection
endpoint](https://connect2id.com/products/server/docs/api/token-introspection):

```ini
op.grantHandler.clientCredentials.simpleHandler.enable=true
op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime=600
op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding=IDENTIFIER
```

Example configuration which puts the registered client metadata fields 
`software_id` and `data` -> `org_id` into the access token `data` field:

```ini
op.grantHandler.clientCredentials.simpleHandler.enable=true
op.grantHandler.clientCredentials.simpleHandler.accessToken.lifetime=600
op.grantHandler.clientCredentials.simpleHandler.accessToken.encoding=IDENTIFIER
op.grantHandler.clientCredentials.simpleHandler.accessToken.includeClientMetadataFields=software_id,data.org_id
```


## Change log

* 1.0 (2014‑09‑13)
    * First official release.

* 1.1 (2014‑11‑12)
    * Upgrades dependencies.

* 1.1.1 (2014‑11‑20)
    * Upgrades dependencies.

* 1.2 (2014‑11‑24)
    * Enables configuration override from Java system properties.

* 1.3 (2015‑03‑09)
    * Add log line IDs.
    * Upgrades dependencies.

* 1.4 (2018‑11‑06)
    * Upgrades to Java 8.

* 2.0 (2020‑12‑06)
    * New op.grantHandler.clientCredentials.simpleHandler.accessToken.includeClientMetadataFields
      configuration property.
    * Gives op.grantHandler.clientCredentials.simpleHandler.enable a default
      setting of false (disabled).
    * Lets op.grantHandler.clientCredentials.simpleHandler.accessToken.audienceList
      also apply to identifier-based access tokens.
    * Makes the /WEB-INF/clientGrantHandler.properties configuration file
      optional.
    * Upgrades to Java 11.
    * Upgrades dependencies.

* 2.0.1 (2022-05-24)
  * Updates dependencies.

* 2.0.2 (2022-06-03)
  * Updates handler to remove use of deprecated APIs.

Questions or comments? Email support@connect2id.com
